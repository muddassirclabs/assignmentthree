package com.example.muddassiriqbal.studentinformationsystem.util;

public interface AppConstants {

    int ADD_REQUEST = 100;
    int EDIT_REQUEST = 200;

    int SORT_BY_NAME = 1;
    int SORT_BY_ROLL = 2;

    String UNSELECTED_BACKGROUND_COLOR = "#141452";
    String WHITE_COLOR = "#FFFFFF";
    String BLACK_COLOR = "#000000";

    String[] DROP_ITEMS = {"Choose an Option", "Name", "Roll Number"};

    String ROLL_NUMBER_EXIST = "This Roll Number already EXIST";
    String ADDED_SUCCESS_MESSAGE = "Successfully Added Entry.";
    String UPDATE_SUCCESS_MESSAGE = "Successfully Updated Entry.";
    String DIALOG_TITLE = "Choose an Option";
    String INAPPROPRIATE_ROLL_MESSAGE = "Roll Number cannot be left blank or can be ZERO!";
    String INAPPROPRIATE_NAME_MESSAGE = "Name cannot Start with Space or Left Blank!";
    String CANCELLED_OPERATION = "Cancelled Operation!";

    String POSITION_KEY = "position";
    String CLICK_KEY = "click";
    String OBJECT_KEY = "student";

    String EDIT_BUTTON = "Edit";
    String VIEW_BUTTON = "View";

}
