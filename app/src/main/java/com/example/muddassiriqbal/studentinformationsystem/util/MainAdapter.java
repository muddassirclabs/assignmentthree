package com.example.muddassiriqbal.studentinformationsystem.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.muddassiriqbal.studentinformationsystem.R;
import com.example.muddassiriqbal.studentinformationsystem.entities.Students;

import java.util.List;

public class MainAdapter extends BaseAdapter{
    List<Students> data;
    Context context;

    public MainAdapter(Context ctx, List<Students> data){
        this.data = data;
        this.context = ctx;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_item,null);
        TextView itemName = (TextView)view.findViewById(R.id.student_name);
        itemName.setText("Name : "+data.get(position).getName());
        TextView itemRoll = (TextView)view.findViewById(R.id.roll_number);
        itemRoll.setText("Roll : "+data.get(position).getRollNo());
        return view;
    }
}
