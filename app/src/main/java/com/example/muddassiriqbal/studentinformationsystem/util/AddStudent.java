package com.example.muddassiriqbal.studentinformationsystem.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muddassiriqbal.studentinformationsystem.R;
import com.example.muddassiriqbal.studentinformationsystem.entities.Students;

import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.CANCELLED_OPERATION;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.CLICK_KEY;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.EDIT_BUTTON;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.INAPPROPRIATE_NAME_MESSAGE;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.INAPPROPRIATE_ROLL_MESSAGE;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.OBJECT_KEY;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.POSITION_KEY;
import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.VIEW_BUTTON;

public class AddStudent extends Activity {

    EditText studentName, rollNumber;
    TextView title;
    Bundle bundle;
    Students student;
    Button save, cancel;
    int position, prevRoll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_student);
        studentName = (EditText) findViewById(R.id.studentname);
        rollNumber = (EditText) findViewById(R.id.rollnumber);
        title = (TextView) findViewById(R.id.main);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.get(CLICK_KEY).equals(VIEW_BUTTON)) {
                save = (Button) findViewById(R.id.save);
                cancel = (Button) findViewById(R.id.cancel);

                student = (Students) bundle.get(OBJECT_KEY);
                studentName.setText(student.getName());
                rollNumber.setText("" + student.getRollNo());
                title.setText("View Student");
                studentName.setEnabled(false);
                rollNumber.setEnabled(false);

                save.setVisibility(View.INVISIBLE);
                cancel.setText("Close");
            } else if (bundle.get(CLICK_KEY).equals(EDIT_BUTTON)) {
                save = (Button) findViewById(R.id.save);
                cancel = (Button) findViewById(R.id.cancel);

                student = (Students) bundle.get(OBJECT_KEY);
                position = bundle.getInt(POSITION_KEY);
                studentName.setText(student.getName());
                prevRoll = student.getRollNo();
                rollNumber.setText("" + prevRoll);
                title.setText("Edit Student");

                save.setText("Update");
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save:
                addStudent();
                break;
            case R.id.cancel:
                Toast.makeText(this, CANCELLED_OPERATION, Toast.LENGTH_SHORT).show();
                setResult(RESULT_CANCELED);
                finish();
            default:
                break;
        }
    }


    private void addStudent() {
        Intent intent = new Intent();
        String name = studentName.getText().toString();
        String roll = rollNumber.getText().toString();
        if (name.equals("") || name.indexOf(" ") == 0) {
            Toast.makeText(this, INAPPROPRIATE_NAME_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (roll.equals("") || Integer.parseInt(roll) == 0) {
            Toast.makeText(this, INAPPROPRIATE_ROLL_MESSAGE, Toast.LENGTH_SHORT).show();
        } else {
            if (bundle != null && bundle.get(CLICK_KEY).equals(EDIT_BUTTON)) {
                name = name.trim();
                intent.putExtra(POSITION_KEY, position);
                intent.putExtra("previous roll", prevRoll);
                intent.putExtra(OBJECT_KEY, new Students(name, Integer.parseInt(roll)));
                setResult(RESULT_OK, intent);
                finish();
            } else {
                name = name.trim();
                intent.putExtra(OBJECT_KEY, new Students(name, Integer.parseInt(roll)));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
