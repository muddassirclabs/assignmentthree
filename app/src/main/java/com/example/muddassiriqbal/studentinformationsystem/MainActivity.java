package com.example.muddassiriqbal.studentinformationsystem;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muddassiriqbal.studentinformationsystem.entities.Students;
import com.example.muddassiriqbal.studentinformationsystem.util.AddStudent;
import com.example.muddassiriqbal.studentinformationsystem.util.MainAdapter;

import static com.example.muddassiriqbal.studentinformationsystem.util.AppConstants.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainActivity extends Activity {
    MainAdapter mainAdapter;
    List<Students> student;
    List<Integer> rollList;
    ListView listView;
    GridView gridView;
    Button viewButton, editButton, deleteButton, listButton, gridButton;
    Spinner dropDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView)findViewById(R.id.listView);
        gridView = (GridView)findViewById(R.id.gridView);
        listButton = (Button)findViewById(R.id.list_view_b);
        gridButton = (Button)findViewById(R.id.grid_view_b);

        // Set Spinner(Drop Down Menu)
        dropDown = (Spinner)findViewById(R.id.spinner);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, DROP_ITEMS){
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;

                // If this is the initial dummy entry, make it hidden
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    //tv.setVisibility(View.GONE);
                    v = tv;
                }
                else {
                    v = super.getDropDownView(position, null, parent);
                }
                return v;
            }
        };
        dropDown.setAdapter(arrayAdapter);

        // Initialize Lists to store student database and roll record
        student = new ArrayList<>();
        rollList = new ArrayList<>();

        // Set adapters for ListView and GridView
        mainAdapter = new MainAdapter(this,student);
        listView.setAdapter(mainAdapter);
        gridView.setAdapter(mainAdapter);

        // Setting Item Click Listeners for ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);
            }
        });

        // Setting Item Click Listeners for GridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);
            }
        });

        // Setting Item Select Listeners for Spinner(Drop Down)
        dropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == SORT_BY_ROLL ){
                    sortByRoll();
                    mainAdapter.notifyDataSetChanged();
                } else if(position == SORT_BY_NAME ){
                    sortByName();
                    mainAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void onClickButton(View view){
        switch (view.getId()){
            case R.id.create_button:
                openAddStudent();
                break;
            case R.id.list_view_b:
                listView.setVisibility(View.VISIBLE);
                gridButton.setBackgroundColor(Color.parseColor(UNSELECTED_BACKGROUND_COLOR));
                gridButton.setTextColor(Color.parseColor(WHITE_COLOR));
                listButton.setBackgroundColor(Color.parseColor(WHITE_COLOR));
                listButton.setTextColor(Color.parseColor(BLACK_COLOR));
                gridView.setVisibility(View.INVISIBLE);
                break;
            case R.id.grid_view_b:
                gridView.setVisibility(View.VISIBLE);
                listButton.setBackgroundColor(Color.parseColor(UNSELECTED_BACKGROUND_COLOR));
                listButton.setTextColor(Color.parseColor(WHITE_COLOR));
                gridButton.setBackgroundColor(Color.parseColor(WHITE_COLOR));
                gridButton.setTextColor(Color.parseColor(BLACK_COLOR));
                listView.setVisibility(View.INVISIBLE);
            default:
                break;
        }
    }

    void openAddStudent(){
        Intent intent = new Intent(this,AddStudent.class);
        startActivityForResult(intent, ADD_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_REQUEST){
            if(resultCode == RESULT_OK){
                Students obj = (Students)data.getSerializableExtra(OBJECT_KEY);
                int roll = obj.getRollNo();
                if(rollList.contains(roll)){
                    Toast.makeText(MainActivity.this, ROLL_NUMBER_EXIST , Toast.LENGTH_SHORT).show();
                }else {
                    student.add(obj);
                    rollList.add(roll);
                    Toast.makeText(MainActivity.this, ADDED_SUCCESS_MESSAGE , Toast.LENGTH_SHORT).show();
                    mainAdapter.notifyDataSetChanged();
                }
            } /*else if(resultCode == RESULT_CANCELED ){ ; }*/
        }else if(requestCode == EDIT_REQUEST ){
            if(resultCode == RESULT_OK){
                Students obj = (Students)data.getSerializableExtra(OBJECT_KEY);
                int pos = data.getIntExtra(POSITION_KEY, student.size());
                int prevRoll = data.getIntExtra("previous roll",rollList.size());
                int roll = obj.getRollNo();
                if(roll == prevRoll){
                    student.set(pos, obj);
                    Toast.makeText(MainActivity.this,UPDATE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                    mainAdapter.notifyDataSetChanged();
                } else if(rollList.contains(roll)){
                    Toast.makeText(MainActivity.this,ROLL_NUMBER_EXIST, Toast.LENGTH_SHORT).show();
                } else {
                    rollList.remove(rollList.indexOf(prevRoll));
                    rollList.add(roll);
                    student.set(pos, obj);
                    Toast.makeText(MainActivity.this,UPDATE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                    mainAdapter.notifyDataSetChanged();
                }
            } /*else if(resultCode == RESULT_CANCELED){ }*/
        }
        sortByName();
        dropDown.setSelection(0);
    }

    public void itemClick(AdapterView<?> parent, View view, final int position, long id){
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.activity_option_dialog);
        dialog.setTitle(DIALOG_TITLE);
        viewButton = (Button)dialog.findViewById(R.id.view);
        editButton = (Button)dialog.findViewById(R.id.edit);
        deleteButton = (Button)dialog.findViewById(R.id.delete);

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,AddStudent.class);
                intent.putExtra(CLICK_KEY, VIEW_BUTTON);
                intent.putExtra(OBJECT_KEY, (Students) mainAdapter.getItem(position));
                startActivity(intent);
                dialog.dismiss();
            }
        });
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,AddStudent.class);
                intent.putExtra(CLICK_KEY, EDIT_BUTTON);
                intent.putExtra(POSITION_KEY, position);
                intent.putExtra(OBJECT_KEY, (Students) mainAdapter.getItem(position));
                startActivityForResult(intent, EDIT_REQUEST);
                dialog.dismiss();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Deleted :"+ student.get(position).getName(), Toast.LENGTH_SHORT).show();
                rollList.remove(rollList.indexOf(student.get(position).getRollNo()));
                student.remove(position);
                mainAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void sortByName(){
        Collections.sort(student,new Comparator<Students>() {
            @Override
            public int compare(Students lhs, Students rhs) {
                return (lhs.getName().compareTo(rhs.getName()));
            }
        });
    }
    public void sortByRoll(){
        Collections.sort(student,new Comparator<Students>() {
            @Override
            public int compare(Students lhs, Students rhs) {
                return (lhs.getRollNo() - rhs.getRollNo());
            }
        });
    }
}
