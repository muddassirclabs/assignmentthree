/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.muddassiriqbal.studentinformationsystem.entities;

import java.io.Serializable;


public class Students implements Serializable{

    //Data members for students
    private String name;
    private int rollNo;

    //constructor that will generate roll number as well
    public Students(String name, int roll) {
        this.name = name;
        rollNo = roll;
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollNo() {
        return rollNo;
    }

}
